---
title: "W1: Interactive map in Leaflet"
author: "Line Stampe-Degn M�ller"
date: 'created 2. February 2022, updated `r format(Sys.time(), "%d %B %Y")`'
output: html_document
---

###### W1: INTERACTIVE MAP IN LEAFLET ######

# 1) Describe a problem or question in your field that lends itself to spatial analysis.

### Within the digital design field, monitoring and analyzing user behavior in public places as an approach to identify a need
### or testing out the effect of an implemented product. As an example, spacial analysis might be an effective tool 
### to understand and analyse patterns in user behavior such as how many times a Voi scooter is picked up and
### left by a user in different areas of the city. By saving the locations and plotting those onto a map,
### the people administration the Voi scooters will be able to identify where it might be beneficial
### to place new Voi parking spots. 

# 2) List data layers that you think are necessary to answer your question/solve your problem. 
# Describe examples of two or three of your listed layers and see if you can find them on the internet.

### To make a spacial analysis of where Voi scooters are picked up and left, 
### feature layers are relevant in order to class geographic features as scooters. 
### It might also me relevant to investigate how the locaion of Voi scooters changes throughout hours of a day
### or during different seasons. For that, scene layers could be a useful tool to visualize changes over time. 
### Source: https://pro.arcgis.com/en/pro-app/latest/help/mapping/layer-properties/layers.htm

# OPTION 1
### Skipped.

# OPTION 2:
# 8) Make a Map: You wish to travel to Chicago for a study stay but wish to stay away 
# from the most crime-ridden areas. You have a friend at Evanston, who invited you in. 
# Is it safe to stay at her place? Make a map that allows both of you to explore the local situation. 
# Use the ChicagoCrime.csv (attached here or on Github) to create an interactive map of murders in the city. 
# Is a heatmap or clustered points better?

### Depending on how many data points are available, both options could be good.
### If there are few data points but still enough that they shouldn't be separate points, clustered
### points might be a good idea.
### If there are so many data points, that the numbers within the clustered points become difficult
### to read and compare instantly by looking at the map, a heatmap might be a better option.
### In this case, there are so many data points that a heatmap might represent the data in the way
### that is easiest to read. However, the clustered points might provide a more exact analysis.

# 9+10) Create a standalone .html map in Leaflet showing at least basic topography and relief, 
# and load in the table of points. Make sure she can see the locations of crime and crime attributes 
# when you hovers over the point markers. 
# Consider adding elements such as minimap() and measure() for easier map interaction.

## Install packages:
install.packages("tidyverse")
install.packages("leaflet")
install.packages("htmlwidgets")

## Libraries:
library(tidyverse)
library(leaflet)
library(htmlwidgets)

## Zoom in on Evanston with setView
leaflet() %>%
  addTiles() %>%
  addProviderTiles("Esri.WorldImagery", 
                   options = providerTileOptions(opacity=0.5)) %>% 
  setView(-87.688568, 42.045597, zoom = 13)
## Providing esri background
l_ev <- leaflet() %>%   # <-- assign the base location to an object
  setView(-87.688568, 42.045597, zoom = 13)
esri <- grep("^Esri", providers, value = TRUE)
for (provider in esri) {
  l_ev <- l_ev %>% addProviderTiles(provider, group = provider)
}
EVmap <- l_ev %>%
  addLayersControl(baseGroups = names(esri),
                   options = layersControlOptions(collapsed = FALSE)) %>%
  addMiniMap(tiles = esri[[1]], toggleDisplay = TRUE,
             position = "bottomright") %>%
  addMeasure(
    position = "bottomleft",
    primaryLengthUnit = "meters",
    primaryAreaUnit = "sqmeters",
    activeColor = "#3D535D",
    completedColor = "#7D4479") %>% 
  htmlwidgets::onRender("
                        function(el, x) {
                        var myMap = this;
                        myMap.on('baselayerchange',
                        function (e) {
                        myMap.minimap.changeLayer(L.tileLayer.provider(e.name));
                        })
                        }") %>% 
  addControl("", position = "topright")

## Save map as a html document 
saveWidget(EVmap, "EVmap.html", selfcontained = TRUE)

## Locations of Crimes
crimes <- read_csv("ChicagoCrimes2017.csv")

## Add markers
leaflet() %>% 
  addTiles() %>% 
  addMarkers(lng = crimes$Longitude, 
             lat = crimes$Latitude,
             popup = crimes$Description,
             label = crimes$`Primary Type`,
             clusterOptions = markerClusterOptions()
  )

# 11) Can you create a heatmap of crime?

### Yes.

## Install packages:
install.packages("leaflet.extras")

## Load library
library(leaflet.extras)
## Clean off data
cleaned_crimes <- crimes %>% 
  filter(!is.na(Latitude)) %>%
  select(c("Latitude","Longitude", "Primary Type", "Community Area"))
## Add heatmap
EVmap %>% 
  addHeatmap(lng = cleaned_crimes$Longitude, lat = cleaned_crimes$Latitude, blur = 40, max = 0.05, radius = 15)

# 12) Explore differentiating the markers (e.g. by color for different kinds of crime)

### Okay.

## Show primary crime types
unique(cleaned_crimes$`Primary Type`)

## Selecting the most dangerous crimes (in my opinon)
danger <- c("ROBBERY", "ASSAULT", "HOMICIDE", "CRIM SEXUAL ASSAULT")

## Function to get the color of a specific crime. 
getColor <- function(danger) {
  case_when(
    danger == "HOMICIDE" ~ "darkgray",
    danger == "ASSAULT" ~ "red",
    danger == "CRIM SEXUAL ASSAULT" ~ "pink",
    danger == "ROBBERY" ~ "orange",
  )
}

## Creates a list of icons for plotting
icons <- awesomeIcons(
  icon = 'ios-close',
  iconColor = 'black',
  library = 'ion',
  markerColor = getColor(crimes$`Primary Type`)
)
EVmap_color <- leaflet() %>% 
  addTiles() %>% 
  addAwesomeMarkers(lng = crimes$Longitude, 
                    lat = crimes$Latitude,
                    popup = paste('Crime:', crimes$`Primary Type`, '<br>',
                                  'Location:', crimes$`Location Description`),
                    icon = icons,
                    clusterOptions = markerClusterOptions())
EVmap_color


# 13) Explore the option of clustering markers with addMarkers(clusterOptions = markerClusterOptions()). 
# Do you recommend marker clustering here?
  
### In question 9, it became necessary to add the clustered markers, since the data set was otherwise too
### large for the code to execute. Thus, it has allready been added.
### Do I reccomend it? Yes, clustered markers makes it much easier to get a clear view of the number of
### crimes committed in each area. In addition, the clustered markers allow you to zoom in on specific 
### areas to view the locations as well as characteristics of each crime in more detail.
### When zooming in, the color of each individual marker will reflect the type of crime.

